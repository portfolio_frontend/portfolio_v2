import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export type User = {
  id: number
  name: string
  email: string
}

export type UsersState = {
  usersById: Record<number, User>
}

export const initialUserState: UsersState = {
  usersById: {}
}

const usersSlice = createSlice({
  name: 'usersSlice',
  initialState: initialUserState,
  reducers: {
    userGetsSucces(state, action: PayloadAction<User[]>) {
      action.payload.forEach(user => {
        state.usersById[user.id] = user
      })
    },
    userGetSuccess(state, action: PayloadAction<User>) {
      const user = action.payload
      state.usersById[user.id] = user
    },
    userDeleteSuccess(state, { payload }: PayloadAction<number>) {
      delete state.usersById[payload]
    }
  }
})

export const {
  userDeleteSuccess,
  userGetsSucces,
  userGetSuccess
} = usersSlice.actions

export default usersSlice.reducer
