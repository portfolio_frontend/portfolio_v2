import { AppThunk } from '@/app/store'
import {
  userGetSuccess,
  User,
  userGetsSucces,
  userDeleteSuccess
} from '@/slicers/users'
import { sleep, apiLatency } from '@/utils'
import { createUser } from '@/slicers/users/mock'
import { setResponse, ResponseProps } from '@/slicers/common'

type UpdateUser = Omit<User, 'id' | 'password'>

type StoreUser = { password: string } & UpdateUser

let userId = 1000

const generateUserId = () => ++userId

const usersRequest = {
  index: () => ({
    request: (): AppThunk => async dispatch => {
      await sleep(apiLatency)

      const users = Array(15)
        .fill(0)
        .map((value, index) => createUser(index + 100))

      dispatch(
        setResponse({
          url: '/api/users/list',
          method: 'get',
          status: 200
        })
      )
      dispatch(userGetsSucces(users))
    }
  }),
  get: (userrId: string) => ({
    request: (): AppThunk => async dispatch => {
      await sleep(apiLatency)
    }
  }),
  post: () => ({
    request: (data: StoreUser): AppThunk => async dispatch => {
      await sleep(apiLatency)

      await dispatch(
        setResponse({
          url: '/api/users',
          method: 'post',
          status: 201
        })
      )
      await dispatch(userGetSuccess({ ...data, id: generateUserId() }))
    },
    success: (): ResponseProps => ({
      url: '/api/users',
      method: 'post',
      status: 201
    })
  }),
  put: (userId: string) => ({
    request: (data: UpdateUser): AppThunk => async dispatch => {
      await sleep(apiLatency)

      await dispatch(
        setResponse({
          url: `/api/users/${userId}`,
          method: 'put',
          status: 200
        })
      )
      await dispatch(userGetSuccess({ ...data, id: Number(userId) }))
    },
    success: (): ResponseProps => ({
      url: `/api/users/${userId}`,
      method: 'put',
      status: 200
    })
  }),
  delete: (userId: string) => ({
    request: (): AppThunk => async dispatch => {
      await sleep(apiLatency)

      await dispatch(
        setResponse({
          url: `api/users/${userId}`,
          method: 'delete',
          status: 200
        })
      )
      await dispatch(userDeleteSuccess(Number(userId)))
    },
    success: (): ResponseProps => ({
      url: `api/users/${userId}`,
      method: 'delete',
      status: 200
    })
  })
}

export default usersRequest
