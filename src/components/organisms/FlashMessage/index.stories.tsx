import React from 'react'
import FlashMessage from './index'
import { withRedux } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { DeepPartial } from '@reduxjs/toolkit'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {}

export default {
  title: base.slice(0, -1),
  component: FlashMessage,
  includeStories: /.*Story$/
}

export const successStory = () => (
  <FlashMessage type="success" message="ユーザーを作成しました" />
)

export const errorStory = () => (
  <FlashMessage type="error" message="ユーザーの作成に失敗しました" />
)
