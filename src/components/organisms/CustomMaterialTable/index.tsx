import React from 'react'
import MaterialTable, { MaterialTableProps } from 'material-table'
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { tableIcons } from './table_icons'

export const theme = createMuiTheme({
  overrides: {
    MuiTableCell: {
      head: {
        background: '#01579b !important'
      }
    },
    MuiTableSortLabel: {
      root: {
        color: 'white !important',
        '&:hover': {
          color: 'white'
        }
      },
      icon: {
        color: 'white !important'
      }
    },
    MuiIconButton: {
      root: {
        '&:hover': {
          backgroundColor: 'inherit'
        }
      }
    },
    MuiTextField: {
      root: {
        marginRight: '3rem'
      }
    }
  }
})

/**
 * MaterialTableに以下の機能を追加
 * ・カラムの表示非表示機能
 */
const CustomMaterialTable = <RowData extends {}>({
  ...props
}: MaterialTableProps<RowData>) => {
  return (
    <ThemeProvider theme={theme}>
      <MaterialTable
        components={{
          ...props.components,
          Container: props => <div {...props}></div>
        }}
        icons={tableIcons}
        {...props}
      />
    </ThemeProvider>
  )
}

export default CustomMaterialTable
