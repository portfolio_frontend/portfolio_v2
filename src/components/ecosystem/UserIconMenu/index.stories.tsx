import React from 'react'
import UserIconMenu from './index'
import { withRedux, withLogin } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { DeepPartial } from '@reduxjs/toolkit'
import { red } from '@material-ui/core/colors'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {}

export default {
  title: base.slice(0, -1),
  component: UserIconMenu,
  decorators: [withLogin()],
  includeStories: /.*Story$/
}

export const defaultStory = () => <UserIconMenu />

export const customizeTextColorStory = () => (
  <UserIconMenu textColor={red[500]} />
)
