import React from 'react'
import { useDispatch, useSelector, shallowEqual } from 'react-redux'
import { RootState } from '@/app/rootReducer'
import clsx from 'clsx'
import { CssBaseline, Box, Paper } from '@material-ui/core'
import AppHeader from '@/components/organisms/AppHeader'
import AppSidebar from '@/components/ecosystem/AppSideBar'
import { toggleSideBar } from '@/slicers/common'
import useStyles from './styles'
import UserIcomMenu from '@/components/ecosystem/UserIconMenu'
import FlashMessageSystem from '../FlashMessageSystem'
import Common from '@/components/Common'
import Loading from '@/components/molecules/Loading'

export const sideBarWidth = 200

export const AppContainer: React.FC = ({ children }) => {
  const classes = useStyles()
  const dispatch = useDispatch()

  const { sideBarOpened } = useSelector((state: RootState) => state.commonState)
  const { loading } = useSelector((state: RootState) => state.commonState)

  const handleDrawer = (open: boolean) => () => {
    dispatch(toggleSideBar(open))
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppHeader
        sideBarWidth={sideBarWidth}
        sideBarOpened={sideBarOpened}
        onSidebarOpen={handleDrawer(true)}
      >
        <UserIcomMenu textColor="white" />
        {/* <LoginLink /> */}
      </AppHeader>
      <AppSidebar
        sideBarWidth={sideBarWidth}
        onSideBarClose={handleDrawer(false)}
        sideBarOpened={sideBarOpened}
      />
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: sideBarOpened
        })}
      >
        <div className={classes.drawerHeader} />
        <Paper elevation={3} className={classes.inner}>
          <Loading visible={loading} />
          <Common />
          <FlashMessageSystem />
          {children}
        </Paper>
      </main>
    </div>
  )
}

export default AppContainer
