import React from 'react'
import MaterialButton, {
  ButtonProps as MaterialButtonProps
} from '@material-ui/core/Button'
import {
  Theme,
  ThemeProvider,
  createMuiTheme,
  makeStyles,
  createStyles
} from '@material-ui/core/styles'

export type PaletteColorProps =
  | 'primary'
  | 'secondary'
  | 'error'
  | 'warning'
  | 'info'
  | 'success'

export type ColorProps = 'grey'

export type CustomButtonProps = {
  color: PaletteColorProps | ColorProps
  component?: React.ReactNode
  to?: string
} & Omit<MaterialButtonProps, 'color'>

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      color: 'white'
    }
  })
)

export const CustomButton: React.FC<CustomButtonProps> = ({
  color,
  variant = 'contained',
  component,
  to,
  ...props
}) => {
  const classes = useStyles()

  return color === 'grey' ? (
    <MaterialButton
      color="default"
      variant={variant}
      {...{ component, to, ...props }}
    />
  ) : (
    // <MaterialButton color="default" variant={variant} {...props} />
    <ThemeProvider
      theme={(theme: Theme) =>
        createMuiTheme({
          ...theme,
          palette: {
            primary: {
              main: theme.palette[color].main
            }
          }
        })
      }
    >
      <MaterialButton
        className={classes.root}
        variant="contained"
        color="primary"
        {...{ component, to, ...props }}
      />
    </ThemeProvider>
  )
}
