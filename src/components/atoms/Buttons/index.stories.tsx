import React from 'react'
import { action } from '@storybook/addon-actions'
import {
  CreateButton,
  EditButton,
  DeleteButton,
  SaveButton,
  LoginButton,
  AddButton,
  BackButton,
  WithIconButton,
  CancelButton
} from './index'
import { Link } from 'react-router-dom'
import { Button as MaterialButton, Box } from '@material-ui/core'
import { People } from '@material-ui/icons'
import DeleteButtonWithDialog from '@/components/organisms/DeleteButtonWithDialog'
import { CustomButton } from '../CustomButton'

const base = require('paths.macro')

export default {
  title: base.slice(0, -1),
  component: CreateButton,
  includeStories: /.*Story$/
}

type ButtonListProps = {
  children: React.ReactElement[]
}
const ButtonList: React.FC<ButtonListProps> = ({ children }) => {
  return (
    <div>
      {children.map(child => (
        <Box mb={1}>{child}</Box>
      ))}
    </div>
  )
}

export const buttonListStory = () => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'row',
      width: '100vw'
    }}
  >
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        marginRight: '1rem'
      }}
    >
      <h2>MaterialUiの標準ボタン</h2>
      <ButtonList>
        <MaterialButton variant="contained" color="default">
          default
        </MaterialButton>
        <MaterialButton variant="contained" color="inherit">
          inherit
        </MaterialButton>
        <MaterialButton variant="contained" color="primary">
          primary
        </MaterialButton>
        <MaterialButton variant="contained" color="secondary">
          secondary
        </MaterialButton>
      </ButtonList>
    </div>
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        marginRight: '1rem'
      }}
    >
      <h2>色</h2>
      <ButtonList>
        <CustomButton color="info">info</CustomButton>
        <CustomButton color="primary">primary</CustomButton>
        <CustomButton color="secondary">secondary</CustomButton>
        <CustomButton color="success">success</CustomButton>
        <CustomButton color="warning">warning</CustomButton>
        <CustomButton color="error">error</CustomButton>
        <CustomButton color="grey">grey</CustomButton>
      </ButtonList>
    </div>
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        marginRight: '1rem'
      }}
    >
      <h2>タイプ</h2>
      <ButtonList>
        <CreateButton onClick={action('clicked')} />
        <AddButton onClick={action('clicked')} />
        <EditButton onClick={action('clicked')} />
        <SaveButton onClick={action('clicked')} />
        <SaveButton onClick={action('clicked')} loading={true} />
        <DeleteButton onClick={action('clicked')} />
        <LoginButton onClick={action('clicked')} />
        <BackButton onClick={action('clicked')} />
        <CancelButton onClick={action('clicked')} />
        <Box>
          {'↓ダイアログで確認するボタン'}
          <DeleteButtonWithDialog onClick={action('clicked')} />
        </Box>
      </ButtonList>
    </div>
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        marginRight: '1rem'
      }}
    >
      <h2>タイプ(loading)</h2>
      <ButtonList>
        <CreateButton loading={true} onClick={action('clicked')} />
        <AddButton loading={true} onClick={action('clicked')} />
        <EditButton loading={true} onClick={action('clicked')} />
        <SaveButton loading={true} onClick={action('clicked')} />
        <SaveButton loading={true} onClick={action('clicked')} />
        <DeleteButton loading={true} onClick={action('clicked')} />
        <LoginButton loading={true} onClick={action('clicked')} />
        <BackButton loading={true} onClick={action('clicked')} />
        <Box>
          {'↓ダイアログで確認するボタン'}
          <DeleteButtonWithDialog onClick={action('clicked')} />
        </Box>
      </ButtonList>
    </div>
  </div>
)

export const IconButtonStory = () => (
  <WithIconButton icon={People} color="info">
    IconButton
  </WithIconButton>
)

export const IconButtonLoadingStory = () => (
  <WithIconButton icon={People} color="info" loading={true}>
    IconButton
  </WithIconButton>
)
