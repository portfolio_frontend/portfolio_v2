import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    icon: {
      marginRight: '.2rem'
    },
    circleIcon: {
      marginRight: '.5rem',
      marginLeft: '.3rem'
    }
  })
)

export default useStyles
