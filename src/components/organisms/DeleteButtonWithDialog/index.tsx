import React, { useState } from 'react'
import DeleteDialog from '@/components/molecules/DeleteDialog'
import { DeleteButton } from '@/components/atoms/Buttons'

type DeleteButtonWithDialogProps = {
  onClick: () => void | Promise<void>
}

const DeleteButtonWithDialog: React.FC<DeleteButtonWithDialogProps> = ({
  onClick
}) => {
  const [opened, setOpened] = useState(false)
  const [loading, setLoading] = useState(false)

  const handleSubmit = async () => {
    await setLoading(true)
    await setOpened(false)
    await onClick()
    await setLoading(false)
  }

  return (
    <div>
      {opened && (
        <DeleteDialog
          isOpened={opened}
          onClickCancelButton={() => setOpened(false)}
          onSubmit={handleSubmit}
        />
      )}
      <DeleteButton onClick={() => setOpened(true)} loading={loading} />
    </div>
  )
}

export default DeleteButtonWithDialog
