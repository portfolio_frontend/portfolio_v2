import { RootState } from '@/app/rootReducer'
import { User } from '.'
import faker from 'faker'
import { initialCommonState } from '@/slicers/common'
import deepmerge from 'deepmerge'

export const createUser = (id: number) =>
  ({
    id,
    email: `${faker.name.firstName()}@example.com`,
    name: faker.name.firstName()
  } as User)

export const withLoginState: Partial<RootState> = {
  commonState: deepmerge(initialCommonState, {
    loginUserId: 1
  }),
  usersState: {
    usersById: {
      1: createUser(1)
    }
  }
}
