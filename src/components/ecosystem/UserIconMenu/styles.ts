import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'

const avatarWith = '2rem'

const useStyles = (textColor: string) =>
  makeStyles((theme: Theme) =>
    createStyles({
      root: {},
      menuContainer: {
        marginTop: '2.5rem'
      },
      avatar: {
        width: avatarWith,
        height: avatarWith
      },
      text: {
        color: textColor
      }
    })
  )

export default useStyles
