import { combineReducers } from '@reduxjs/toolkit'

import usersState from '@/slicers/users'
import commonState from '@/slicers/common'

const rootReducer = combineReducers({
  commonState,
  usersState
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
