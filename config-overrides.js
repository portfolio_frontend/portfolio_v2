
const {
  override,
  addWebpackAlias,
  useBabelRc,
} = require("customize-cra");
const path = require("path");
const { addReactRefresh } = require("customize-cra-react-refresh");

module.exports = override(
  addReactRefresh({ disableRefreshCheck: true }),
  addWebpackAlias({
    ["@"]: path.resolve(__dirname, './src')
  }),
  useBabelRc()
)

