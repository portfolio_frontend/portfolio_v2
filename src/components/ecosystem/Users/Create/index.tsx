import React, { useEffect } from 'react'
import UserForm, { UserFormData } from '@/components/organisms/UserForm'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import usersRequest from '@/api/users'
import { RootState } from '@/app/rootReducer'
import { setFlashSuccess } from '@/slicers/common'
import isequal from 'lodash.isequal'

const UsersCreate: React.FC = ({}) => {
  const dispatch = useDispatch()
  const api = usersRequest.post()
  const history = useHistory()
  const { response } = useSelector((state: RootState) => state.commonState)

  const handleSubmit = async (data: UserFormData) => {
    await dispatch(api.request(data))
  }

  useEffect(() => {
    const func = async () => {
      if (isequal(response, api.success())) {
        await history.push('/users/list')
        await dispatch(setFlashSuccess('ユーザーを作成しました'))
      }
    }
    func()
  }, [response])

  return (
    <UserForm onSubmit={handleSubmit} password={true} backUrl={'/users/list'} />
  )
}

export default UsersCreate
