import React from 'react'
import LoginForm from './index'
import { withRedux, withStyles } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { action } from '@storybook/addon-actions'
import { DeepPartial } from '@reduxjs/toolkit'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {}

export default {
  title: base.slice(0, -1),
  component: LoginForm,
  decorators: [withStyles({ width: '30vw' }), withRedux(preloadState)],
  includeStories: /.*Story$/
}

export const defaultStory = () => (
  <LoginForm loading={false} onSubmit={action('submit')} />
)
