import React from 'react'
import '@testing-library/jest-dom'
import { render, fireEvent, screen, waitFor } from '@testing-library/react'
import DeleteDialog from './index'
import AppWrapper from '@/AppWrapper'

jest.spyOn(window, 'alert').mockImplementation(() => {})

const setup = async (isOpened: boolean) => {
  const handleClickCancelButton = jest.fn()
  const handleSubmit = jest.fn()

  const utils = render(
    <AppWrapper>
      <DeleteDialog
        isOpened={isOpened}
        onClickCancelButton={handleClickCancelButton}
        onSubmit={handleSubmit}
      />
    </AppWrapper>
  )

  const dialogText = '本当に削除しますか'
  const handleAlert = (window.alert = jest.fn())

  const changeInput = (value: string) =>
    fireEvent.change(utils.getByPlaceholderText('Delete'), {
      target: { value }
    })

  const clickCancel = () => fireEvent.click(utils.getAllByRole('button')[0])

  const clickSubmit = () => fireEvent.click(utils.getAllByRole('button')[1])

  return {
    utils,
    handleSubmit,
    clickSubmit,
    clickCancel,
    changeInput,
    dialogText,
    handleClickCancelButton,
    handleAlert
  }
}

test('opened', async () => {
  const utils = await setup(true)

  expect(screen.queryByText(utils.dialogText)).toBeInTheDocument()
})

test('closed', async () => {
  const utils = await setup(false)

  expect(screen.queryByText(utils.dialogText)).toBeNull()
})

test('cancel button clicked', async () => {
  const utils = await setup(true)

  utils.clickCancel()

  expect(utils.handleClickCancelButton).toHaveBeenCalled()
})

test('wrong input text', async () => {
  const utils = await setup(true)

  utils.changeInput('wrong')
  utils.clickSubmit()

  await waitFor(() => expect(utils.handleAlert).toHaveBeenCalled())
})

test('succcess', async () => {
  const utils = await setup(true)

  utils.changeInput('Delete')
  utils.clickSubmit()

  await waitFor(() => expect(utils.handleSubmit).toHaveBeenCalled())
})
