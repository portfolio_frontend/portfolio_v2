import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Color } from '@material-ui/lab/Alert'
import { AppThunk } from '@/app/store'

export type HttpMethod = 'get' | 'post' | 'put' | 'delete'

export type ValidationErrrorResponseProps = {
  message: string
  errors?: { [key: string]: Array<string> }
}

export type FlashProps = {
  type: Color
  message: string | null
}

export type ResponseProps = {
  url: string | null
  method: HttpMethod
  status: number
}

export type CommonState = {
  sideBarOpened: boolean
  flash: FlashProps
  loginUserId: number | null
  loading: boolean
  response: ResponseProps
}

export const initialFlashState: FlashProps = {
  message: null,
  type: 'info'
}

export const initialResponseState: ResponseProps = {
  method: 'get',
  url: null,
  status: 0
}

export const initialCommonState: CommonState = {
  sideBarOpened: true,
  flash: initialFlashState,
  loginUserId: null,
  loading: false,
  response: initialResponseState
}

const adminContainerSlice = createSlice({
  name: 'adminContainer',
  initialState: initialCommonState,
  reducers: {
    toggleSideBar(state, acttion: PayloadAction<boolean>) {
      state.sideBarOpened = acttion.payload
    },
    setFlash(state, action: PayloadAction<CommonState['flash']>) {
      state.flash.message = null
      state.flash = action.payload
    },
    initFlash(state) {
      state.flash.message = ''
      state.flash.type = 'info'
    },
    setLoginUserId(state, action: PayloadAction<CommonState['loginUserId']>) {
      state.loginUserId = action.payload
    },
    setLoading(state, { payload }: PayloadAction<boolean>) {
      state.loading = payload
    },
    setResponse(state, action: PayloadAction<CommonState['response']>) {
      state.response = { ...action.payload }
    },
    initResponse(state) {
      state.response = initialResponseState
    }
  }
})

export const {
  toggleSideBar,
  setLoginUserId,
  initFlash,
  setLoading,
  initResponse,
  setResponse
} = adminContainerSlice.actions

export default adminContainerSlice.reducer

export const setFlashSuccess = (
  message: string
): AppThunk => async dispatch => {
  dispatch(initFlash())
  dispatch(
    adminContainerSlice.actions.setFlash({
      type: 'success',
      message
    })
  )
}

export const setFlashError = (message: string): AppThunk => async dispatch => {
  dispatch(initFlash())

  dispatch(
    adminContainerSlice.actions.setFlash({
      type: 'error',
      message
    })
  )
}

export const initCommonStates = (): AppThunk => async dispatch => {
  dispatch(initFlash())
  dispatch(setLoading(false))
}
