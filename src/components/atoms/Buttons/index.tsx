import React from 'react'
import styles from './styles.module.scss'
import {
  Create,
  Edit,
  Delete,
  Save,
  Details,
  ExitToApp,
  ArrowBack,
  Add,
  Cancel
} from '@material-ui/icons'
import { CircularProgress } from '@material-ui/core'
import { ThemeProvider, Theme, createMuiTheme } from '@material-ui/core/styles'
import { CustomButtonProps, CustomButton } from '../CustomButton'
import { grey } from '@material-ui/core/colors'
import useStyles from './styles'

type WithIconButtonProps = {
  icon: (props: any) => JSX.Element
  loading?: boolean
} & CustomButtonProps

type ButtonProps = Omit<WithIconButtonProps, 'icon' | 'color'>

export const WithIconButton: React.FC<WithIconButtonProps> = ({
  icon: Icon,
  children,
  loading = false,
  ...props
}) => {
  const classes = useStyles()

  return (
    <ThemeProvider
      theme={(theme: Theme) =>
        createMuiTheme({
          overrides: {
            MuiButton: {
              root: {
                '&$disabled': {
                  color:
                    props.color === 'grey'
                      ? 'black'
                      : grey[200] + ' !important',
                  backgroundColor:
                    props.color === 'grey'
                      ? ''
                      : theme.palette[props.color].dark + ' !important'
                }
              }
            }
          }
        })
      }
    >
      <CustomButton color="warning" {...props} disabled={loading}>
        {loading ? (
          <CircularProgress
            data-testid="loadingIcon"
            className={classes.circleIcon}
            size="1rem"
          />
        ) : (
          <Icon data-testid="icon" className={classes.icon} />
        )}
        {children}
      </CustomButton>
    </ThemeProvider>
  )
}

export const CreateButton: React.FC<ButtonProps> = ({
  children = '作成',
  ...props
}) => (
  <WithIconButton color="info" icon={Create} {...props}>
    {children}
  </WithIconButton>
)

export const AddButton: React.FC<ButtonProps> = ({
  children = '追加',
  ...props
}) => (
  <WithIconButton color="info" icon={Add} {...props}>
    {children}
  </WithIconButton>
)

export const EditButton: React.FC<ButtonProps> = ({
  children = '編集',
  ...props
}) => (
  <WithIconButton color="primary" icon={Edit} {...props}>
    {children}
  </WithIconButton>
)

export const DetailButton: React.FC<ButtonProps> = ({
  children = '詳細',
  ...props
}) => (
  <WithIconButton color="primary" icon={Details} {...props}>
    {children}
  </WithIconButton>
)

export const DeleteButton: React.FC<ButtonProps> = ({
  children = '削除',
  ...props
}) => (
  <WithIconButton color="secondary" icon={Delete} {...props}>
    {children}
  </WithIconButton>
)

export const SaveButton: React.FC<ButtonProps> = ({
  children = '保存',
  ...props
}) => (
  <WithIconButton color="success" icon={Save} {...props}>
    {children}
  </WithIconButton>
)

export const LoginButton: React.FC<ButtonProps> = ({
  children = 'ログイン',
  ...props
}) => (
  <WithIconButton color="warning" icon={ExitToApp} {...props}>
    {children}
  </WithIconButton>
)

export const BackButton: React.FC<ButtonProps> = ({
  children = '戻る',
  ...props
}) => (
  <WithIconButton
    color="grey"
    icon={ArrowBack}
    {...props}
    style={{ padding: '6px 8px' }}
  >
    {children}
  </WithIconButton>
)

export const CancelButton: React.FC<ButtonProps> = ({
  children = 'キャンセル',
  ...props
}) => (
  <WithIconButton
    color="grey"
    icon={Cancel}
    {...props}
    style={{ padding: '6px 8px' }}
  >
    {children}
  </WithIconButton>
)
