import React from 'react'
import styles from './styles.module.scss'
import { TextField } from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { LoginButton } from '@/components/atoms/Buttons'
import useStyles from './styles'

export type FormData = {
  name: string
  password: string
}

export type LoginFormProps = {
  onSubmit: (formData: FormData) => void
  loading: boolean
}

const LoginForm: React.FC<LoginFormProps> = ({
  loading,
  onSubmit: _onSubmit
}) => {
  const classes = useStyles()
  const { register, handleSubmit, errors } = useForm<FormData>()

  const onSubmit = handleSubmit(props => {
    _onSubmit(props)
  })

  return (
    <form onSubmit={onSubmit}>
      <div>
        <div className={classes.root}>
          <TextField
            required
            id="standard-required"
            label="email"
            name="name"
            inputRef={register({ required: true })}
          />
          {errors.name && 'name is required.'}
          <TextField
            required
            id="standard-password-input"
            label="Password"
            type="password"
            autoComplete="current-password"
            name="password"
            inputRef={register({ required: true })}
          />
          {errors.password && 'password is required.'}
        </div>
      </div>
      <div className={classes.buttonContainer}>
        <LoginButton loading={loading} type="submit" />
      </div>
    </form>
  )
}

export default LoginForm
