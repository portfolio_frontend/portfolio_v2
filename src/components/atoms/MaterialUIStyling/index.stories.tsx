import React from 'react'
import { ThemeNestingExtend, AntSwitch, OverrideThemeExample } from './index'
import { withRedux } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { DeepPartial } from '@reduxjs/toolkit'
import { TextField, Switch } from '@material-ui/core'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {}

export default {
  title: base.slice(0, -1),
  decorators: [withRedux(preloadState)],
  includeStories: /.*Story$/
}

export const themeNestingExpandStory = () => <ThemeNestingExtend />

export const overrideThemeExampleStory = () => <OverrideThemeExample />

export const antSwitchStory = () => (
  <div>
    <AntSwitch />
    <Switch />
  </div>
)
