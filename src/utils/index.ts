export const apiLatency = 1000

export const sleep = async (milliseconds: number) => {
  await new Promise(resolve => setTimeout(resolve, milliseconds))
}
