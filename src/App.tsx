
import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
  RouteProps,
  useLocation,
  Redirect
} from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '@/app/rootReducer'
import LoginPage from '@/components/ecosystem/LoginPage'
import AppContainer from '@/components/ecosystem/AppContainer'
import Common from './components/Common'
import FlashMessageSystem from './components/ecosystem/FlashMessageSystem'
import UsersList from './components/ecosystem/Users/List'
import UsersShow from './components/ecosystem/Users/Show'
import UsersCreate from './components/ecosystem/Users/Create'

export type UsersRouteProps = {
  user: string
}

export default function AuthExample() {
  return (
    <Router>
      <div>
        <Common />
        <Switch>
          <Route path="/login">
            <React.Fragment>
              <FlashMessageSystem />
              <LoginPage />
            </React.Fragment>
          </Route>
          <PrivateRoute>
            <AppContainer>
              <Route exact path={'/users/list'} component={UsersList} />
              <Route exact path={'/users/:user/show'} component={UsersShow} />
              <Route exact path={'/users/create'} component={UsersCreate} />
            </AppContainer>
          </PrivateRoute>
          <Route path="*">
            <NoMatch />
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

const PrivateRoute: React.FC<RouteProps> = ({ children }) => {
  const dispatch = useDispatch()
  const location = useLocation()

  const { loginUserId } = useSelector((state: RootState) => state.commonState)
  const history = useHistory()

  if (location.pathname === '/') {
    history.push('/login')
  }

  //   useEffect(() => {
  //     const func = async () => {
  //       await dispatch(setLoginLoading(true))
  //       await dispatch(authRequest.fetchLoginUser())
  //       await dispatch(setLoginLoading(false))
  //     }
  //     if (!isLogin) {
  //       func()
  //     }
  //   }, [])

  return loginUserId ? (
    <Route render={({ location }) => children} />
  ) : (
    // <Loading visible={true} />
    <Redirect to="/login" />
  )
}

export function NoMatch() {
  let location = useLocation()

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  )
}
