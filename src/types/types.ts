export interface ValidationErrorResponse {
  message: string
  errors?: { [key: string]: Array<string> }
}

export type Story = {
  story?: {
    decorators?: ((storyFn: Function) => JSX.Element)[]
  }
}
