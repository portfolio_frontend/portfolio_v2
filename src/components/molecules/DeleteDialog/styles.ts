import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      color: theme.palette.secondary.main
    },
    body: {
      margin: 0
    }
  })
)

export default useStyles
