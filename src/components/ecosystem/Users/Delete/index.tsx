import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams, useHistory } from 'react-router'
import DeleteButtonWithDialog from '@/components/organisms/DeleteButtonWithDialog'
import { UsersRouteProps } from '@/App'
import usersRequest from '@/api/users'
import { setFlashSuccess } from '@/slicers/common'
import isqeual from 'lodash.isequal'
import { RootState } from '@/app/rootReducer'

export const UsersDelete: React.FC = ({}) => {
  const { user: userId } = useParams<UsersRouteProps>()
  const dispatch = useDispatch()
  const api = usersRequest.delete(userId)
  const [loading, setLoading] = useState(false)
  const history = useHistory()
  const { response } = useSelector((state: RootState) => state.commonState)

  const handleClick = async () => {
    await setLoading(true)
    await dispatch(api.request())
  }

  useEffect(() => {
    const func = async () => {
      if (isqeual(response, api.success())) {
        await history.push('/users/list')
        await dispatch(setFlashSuccess('ユーザーを削除しました'))
      }
    }
    func()
  }, [response])

  return <DeleteButtonWithDialog onClick={handleClick} />
}

export default UsersDelete
