import React from 'react'
import {
  createMuiTheme,
  ThemeProvider,
  Theme,
  withStyles
} from '@material-ui/core/styles'
import Checkbox from '@material-ui/core/Checkbox'
import { green, orange, red, yellow, blue } from '@material-ui/core/colors'
import { Button, TextField, ExpansionPanel, Switch } from '@material-ui/core'
import { antSwitchUseStyle } from './styles'

/**
 * 親要素にthemeを設定するとその子要素全てに反映される
 *
 * createMuiThemeは
 *     オブジェクトを渡すとdefaultのthemeとマージされる
 *     関数を渡すと親のthemeを取得できるので、親のthemeを上書きできる
 */
export const ThemeNestingExtend = () => {
  return (
    //   theme作成
    <ThemeProvider
      theme={createMuiTheme({
        palette: {
          primary: {
            main: green[500]
          },
          secondary: {
            main: yellow[500]
          }
        }
      })}
    >
      <div>
        <p>outerTheme</p>
        <Checkbox defaultChecked color="primary" />
        <Checkbox defaultChecked color="secondary" />

        {/* outerのthemeを一部上書きする */}
        <ThemeProvider
          theme={(outerTheme: Theme) =>
            createMuiTheme({
              ...outerTheme,
              palette: {
                ...outerTheme.palette,
                secondary: {
                  main: red[500]
                }
              }
            })
          }
        >
          <p>expandOuterTheme</p>
          <Checkbox defaultChecked color="primary" />
          <Checkbox defaultChecked color="secondary" />
        </ThemeProvider>

        {/* 新しいthemeを作成 */}
        <ThemeProvider
          theme={createMuiTheme({
            palette: {
              secondary: {
                main: red[500]
              }
            }
          })}
        >
          <p>newTheme</p>
          <Checkbox defaultChecked color="primary" />
          <Checkbox defaultChecked color="secondary" />
        </ThemeProvider>
      </div>
    </ThemeProvider>
  )
}

/**
 * overrideを使用してスタイリング
 *
 */
export const OverrideThemeExample = () => {
  return (
    <div>
      <ThemeProvider
        theme={createMuiTheme({
          overrides: {
            MuiButton: {
              root: {
                backgroundColor: 'green'
              }
            }
          }
        })}
      >
        <Button color="default">test</Button>
      </ThemeProvider>
      <ThemeProvider
        theme={createMuiTheme({
          overrides: {
            MuiButton: {
              root: {
                backgroundColor: 'red'
              }
            }
          }
        })}
      >
        <Button color="default">test</Button>
      </ThemeProvider>
    </div>
  )
}

/**
 * makeStyles
 */
export const AntSwitch = () => {
  const classes = antSwitchUseStyle()

  return <Switch classes={classes} />
}

/**
 *
 * withStyles
 */
const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: 'red'
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'red'
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'red'
      },
      '&:hover fieldset': {
        borderColor: 'red'
      },
      '&.Mui-focused fieldset': {
        borderColor: 'red'
      }
    }
  }
})(TextField)

export default function CustomizedInputs() {
  return (
    <form noValidate>
      <CssTextField id="custom-css-standard-input" label="Custom CSS" />
    </form>
  )
}
