import React from 'react'
import { Button as MaterialButton, Box } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import { useForm } from 'react-hook-form'
import { DeleteButton } from '@/components/atoms/Buttons'
import useStyles from './styles'

const keyword = 'Delete'

type FormProps = {
  input: string
}

type DeleteDialogProps = {
  isOpened: boolean
  onSubmit: () => void | Promise<void>
  onClickCancelButton: () => void
}

const DeleteDialog: React.FC<DeleteDialogProps> = ({
  isOpened,
  onClickCancelButton,
  onSubmit
}) => {
  const classes = useStyles()
  const { register, handleSubmit } = useForm<FormProps>()

  const handleClickDeleteButton = handleSubmit(async props => {
    if (props.input !== keyword) {
      return alert('入力内容が間違っています')
    }

    await onSubmit()
  })

  return (
    <div>
      <Dialog open={isOpened} aria-labelledby="form-dialog-title">
        <DialogContent>
          <Box component={DialogContentText} className={classes.title}>
            本当に削除しますか
          </Box>
          <DialogContentText className={classes.body}>
            削除する場合は{keyword}と入力して削除ボタンを押してください。
          </DialogContentText>
          <DialogContentText className={classes.body}>
            キャンセルする場合はキャンセルボタンを押してください。
          </DialogContentText>

          <TextField
            autoFocus
            margin="dense"
            id="name"
            name="input"
            placeholder={keyword}
            fullWidth
            inputRef={register({ required: true })}
          />
        </DialogContent>
        <DialogActions>
          <MaterialButton
            onClick={onClickCancelButton}
            color="default"
            variant="contained"
          >
            キャンセル
          </MaterialButton>
          <DeleteButton onClick={handleClickDeleteButton} />
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default DeleteDialog
