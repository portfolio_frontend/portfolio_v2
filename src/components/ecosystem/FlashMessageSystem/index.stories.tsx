import React from 'react'
import FlashMessageSystem from './index'
import { withRedux } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { DeepPartial } from 'redux'
import { FlashProps } from '@/slicers/common'
const base = require('paths.macro')

export const flashMessage: FlashProps = {
  message: '作成しました',
  type: 'success'
}

const preloadState: DeepPartial<RootState> = {
  commonState: {
    flash: flashMessage
  }
}

export default {
  title: base.slice(0, -1),
  component: FlashMessageSystem,
  decorators: [withRedux(preloadState)],
  includeStories: /.*Story$/
}

export const defaultStory = () => <FlashMessageSystem />
