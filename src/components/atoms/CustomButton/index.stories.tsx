import React from 'react'
import { CustomButton } from './index'
import { withRedux } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { DeepPartial } from '@reduxjs/toolkit'
import { Box } from '@material-ui/core'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {}

export default {
  title: base.slice(0, -1),
  decorators: [withRedux(preloadState)],
  includeStories: /.*Story$/
}

export const defaultStory = () => (
  <Box>
    <CustomButton color="error">error</CustomButton>
    <CustomButton color="info">info</CustomButton>
    <CustomButton color="primary">primary</CustomButton>
    <CustomButton color="secondary">secondary</CustomButton>
    <CustomButton color="success">success</CustomButton>
    <CustomButton color="grey">grey</CustomButton>
  </Box>
)
