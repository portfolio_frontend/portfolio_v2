import { createSelector } from 'reselect'
import { RootState } from '@/app/rootReducer'

export const getLoginUserSelector = createSelector(
  (state: RootState) => state.commonState.loginUserId,
  (state: RootState) => state.usersState.usersById,
  (loginUserId, users) => {
    if (!loginUserId) return null

    const loginUser = users[loginUserId]
    return loginUser
  }
)
