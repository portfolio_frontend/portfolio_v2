import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styles from './styles.module.scss'
import LoginForm, { LoginFormProps } from '@/components/organisms/LoginForm'
import { Redirect } from 'react-router-dom'
import { RootState } from '@/app/rootReducer'
import authRequest from '@/api/auth'
import useStyles from './styles'

const LoginPage = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { loginUserId } = useSelector((state: RootState) => state.commonState)
  const [loading, setLogding] = useState(false)
  const handleSubmit: LoginFormProps['onSubmit'] = async formData => {
    await setLogding(true)
    await dispatch(authRequest.login().request(formData))
    await setLogding(false)
  }

  return loginUserId ? (
    <Redirect to={'/users/list'} />
  ) : (
    <div className={classes.root}>
      <LoginForm loading={loading} onSubmit={handleSubmit} />
    </div>
  )
}

export default LoginPage
