import React from 'react'
import CustomMaterialTable from './index'
import { withRedux } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { DeepPartial } from '@reduxjs/toolkit'
import {
  DetailButton,
  LoginButton,
  AddButton
} from '@/components/atoms/Buttons'
import { Box } from '@material-ui/core'
import { Link } from 'react-router-dom'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {}

export default {
  title: base.slice(0, -1),
  component: CustomMaterialTable,
  decorators: [withRedux(preloadState)],
  includeStories: /.*Story$/
}

export const defaultStory = () => (
  <div style={{ margin: '1rem' }}>
    <CustomMaterialTable
      title="Cell Header Styling Preview"
      data={[
        {
          id: 1,
          name: 'Mehmet',
          surname: 'Baran',
          birthYear: 1987,
          birthCity: 63
        },
        {
          id: 2,
          name: 'Zerya Betül',
          surname: 'Baran',
          birthYear: 2017,
          birthCity: 34
        }
      ]}
      actions={[
        {
          isFreeAction: true,
          icon: () => <AddButton />,
          onClick: event => null
        }
      ]}
      columns={[
        {
          title: 'ID',
          field: 'id'
        },
        {
          title: 'Name',
          field: 'name'
        },
        { title: 'Surname', field: 'surname' },
        { title: 'Birth Year', field: 'birthYear', type: 'numeric' },
        {
          title: 'Birth Place',
          field: 'birthCity',
          lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' }
        },
        {
          title: 'Avatar',
          field: 'imageUrl',
          render: (rowData: any) => (
            <div>
              {rowData.id}
              <DetailButton />
              <LoginButton />
            </div>
          )
        }
      ]}
    />
  </div>
)
